//
// Created by JK on 2021/01/09.
//

import UIKit

// swiftlint:disable type_name force_unwrapping
enum R {
  enum Color {
    static let systemBackground = UIColor(named: "systemBackground")!
    static let applicationBackground = UIColor(named: "applicationBackground")!
    static let systemGray6 = UIColor(named: "systemGray6")!
    static let defaultBackground = UIColor(named: "defaultBackground")!
    static let label = UIColor(named: "label")!
  }
}
